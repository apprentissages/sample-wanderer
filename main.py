import json
from wanderer import Wanderer
from logger import get_logger
from args import get_args


def perform():
    logger = get_logger()
    args = get_args()
    w = Wanderer(args.category)
    result = w.search()
    print(json.dumps(result, ensure_ascii=False, indent=4, sort_keys=True))


if __name__ == '__main__':
    perform()
