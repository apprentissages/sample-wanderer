from manipulator import TextManipulator
from manipulator import MovieManipulator
from manipulator import BookManipulator
from manipulator import BlogManipulator
from manipulator import NewsManipulator


class Wanderer:
    def __init__(self, category):
        self.category = category
        self.text = TextManipulator(category=self.category)
        self.movie = MovieManipulator(category=self.category)
        self.book = BookManipulator(category=self.category)
        self.blog = BlogManipulator(category=self.category)
        self.news = NewsManipulator(category=self.category)

    def search(self):
        return {
            'web': self.text.execute(),
            'movie': [],
            'book': [],
            'blog': [],
            'news': [],
        }
