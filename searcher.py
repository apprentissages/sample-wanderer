import requests
from urllib import parse


class TextSearcher:
    def execute(self, **kwargs):
        url = f'https://html.duckduckgo.com/html/?q={parse.quote(kwargs.get("category"))}'
        return requests.get(url, headers={'user-agent': 'my-app/0.0.1'})
