import argparse

def get_args():
    parser = argparse.ArgumentParser(description='options')
    parser.add_argument('--category', '-c', dest='category', type=str,
                        required=True, help='category name')
    return parser.parse_args()
