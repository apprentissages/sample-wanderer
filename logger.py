import sys
from logging import getLogger
from logging import Formatter
from logging import BASIC_FORMAT
from logging import StreamHandler
from logging import DEBUG

def get_logger():
    logger = getLogger(__name__)
    logger.setLevel(DEBUG)
    formatter = Formatter(BASIC_FORMAT)
    handler = StreamHandler(sys.stdout)
    handler.setLevel(DEBUG)
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    return logger
