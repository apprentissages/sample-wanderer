from bs4 import BeautifulSoup
from urllib import parse



class TextParser:
    def execute(self, **kwargs):
        soup = BeautifulSoup(kwargs.get('response').content, 'html.parser')
        selected = soup.select('div[id=links] h2 a')

        def create_row(anchor):
            params = parse.parse_qs(parse.urlparse(
                anchor.attrs.get('href')).query)
            return {
                'link': params.get('uddg')[0],
                'title': anchor.text
            }
        return [create_row(r) for r in selected]
