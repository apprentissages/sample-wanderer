from searcher import TextSearcher
from parser import TextParser


class Manipulator:
    def __init__(self, **kwargs):
        self.category = kwargs.get('category')


class TextManipulator(Manipulator):
    def execute(self):
        self.searcher = TextSearcher()
        self.parser = TextParser()
        self.response = self.search()
        self.result = self.parse()
        return self.result

    def search(self):
        return self.searcher.execute(category=self.category)

    def parse(self):        
        return self.parser.execute(response=self.response)


class MovieManipulator(Manipulator):
    """TODO: not implemented."""
    pass


class BookManipulator(Manipulator):
    """TODO: not implemented."""
    pass


class BlogManipulator(Manipulator):
    """TODO: not implemented."""
    pass


class NewsManipulator(Manipulator):
    """TODO: not implemented."""
    pass
